import React from "react";
import { Animated, Image, Text, View } from "react-native";
import { ITEM_SIZE, SPACING } from "./index";
import { AppIcon } from "../../lib/IconUtils";
import { Icons } from "../../icons/common";
import { defaultPadding, defaultPaddingRegular } from "../../constants";

const DetailsItem = (props: any) => {
    return (
        <View style={{ flexDirection: "row", flex: 1, alignItems: "center" }}>
            <Image source={props.imagePath} style={{width: 15, height: 15}}/>
            <View style={{ flex: 1, paddingHorizontal: defaultPadding}}>
                <Text style={{fontSize: 10, fontWeight: "600", color: "#102654"}}> {props.title} </Text>
                <Text style={{fontSize: 12, fontWeight: "600", color: "#464646"}}> {props.value} </Text>
            </View>
        </View>
    );
}

export const CarouselItem = (props: any) => {
    const { item, itemIndex, scrollX} = props;
    const inputRange = [
        (itemIndex - 2) * ITEM_SIZE,
        (itemIndex - 1) * ITEM_SIZE,
        itemIndex * ITEM_SIZE,
    ];

    const scale = scrollX.interpolate({
        inputRange,
        outputRange: [ .95, 1, .95],
    });

    const opacity = scrollX.interpolate({
        inputRange,
        outputRange: [ .75, 1, .75],
    });

    return (
        <View style={{ width: ITEM_SIZE, }}>
            <Animated.View
                style={{
                    marginHorizontal: - 2 * SPACING,
                    transform: [{ scale } ],
                    borderRadius: 34,
                    opacity
                }}
            >
                <View style={{backgroundColor: "white", height: ITEM_SIZE * .5, borderRadius: 24, overflow: "hidden", borderWidth: 1, borderColor: "#C6CFD9"}}>
                    <View style={{backgroundColor: "#F7F5F8", height: ITEM_SIZE * .25}}>
                       <View style={{flexDirection: "row"}}>
                           <View style={{ marginRight: 10, width: 90, flexDirection: "row"}}>
                               <Image source={require("../../assets/left_top.png")} style={{width: 60, height: 70}}/>
                           </View>
                           <View style={{flex: 1, justifyContent: "center"}}>
                               <Text style={{fontSize: 14, fontWeight: "600", color: "#686869"}}>
                                   Ref Number: 7468902-WL
                               </Text>
                               <Text style={{fontSize: 16, fontWeight: "600", color: "#102654"}}>
                                   Win Mercedes GTS 2020
                               </Text>
                           </View>
                           <View style={{marginTop: -1}}>
                               <AppIcon
                                   name={"top_right"}
                                   provider={Icons}
                                   size={40}/>
                               <Text style={{fontSize: 16, fontWeight: "600", color: "#E03A3F", position: "absolute", top: 10, right: 15}}>
                                   1
                               </Text>
                           </View>
                       </View>
                    </View>

                    <View style={{ flexDirection: "row", alignItems: "center", height: ITEM_SIZE * .25, paddingHorizontal: defaultPaddingRegular}}>
                        <View>
                            <Image source={require("../../assets/shirt.png")} style={{width: 50, height: 50}}/>
                        </View>
                        <View style={{flex: 1, paddingHorizontal: defaultPaddingRegular}}>
                            <DetailsItem imagePath={require("../../assets/calendar.png")} title={"Date purchased"} value={"18/02/2020"}/>
                            <DetailsItem imagePath={require("../../assets/ticket1.png")} title={"Ticket(s) Bought"} value={"2"}/>
                        </View>
                        <Image source={require("../../assets/gold.png")} style={{width: 100, height: 100, position: "absolute", top: -20, right: 15}}/>
                    </View>
                </View>
            </Animated.View>
        </View>
    );
}

export default CarouselItem;
