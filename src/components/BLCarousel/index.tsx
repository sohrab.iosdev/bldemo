import React from "react";
import { Animated, Dimensions, View } from "react-native";
import CarouselItem from "./CarouselItem";

export const SPACING = 10;
const { width } = Dimensions.get('window');
export const ITEM_SIZE = width * 0.82;
const EMPTY_ITEM_SIZE = (width - ITEM_SIZE) / 2;

export const BLCarousel = (props: any) => {
    const { items } = props;
    const [currPage, setCurrPage] = React.useState(1);
    const scrollX = React.useRef(new Animated.Value(0)).current;
    let onScrollEnd = (e) => {
        let pageNumber = Math.min(Math.max(Math.floor(e.nativeEvent.contentOffset.x / ITEM_SIZE + 0.5) + 1, 0), items.length);
         setCurrPage(pageNumber);
    }

    let onScrollDrag = (e) => {
        setCurrPage(currPage + 1);
    }

    return (
        <View style={{}}>
            <Animated.FlatList
                showsHorizontalScrollIndicator={false}
                data={items}
                keyExtractor={(item) => item.key}
                horizontal
                bounces={false}
                decelerationRate={0}
                contentContainerStyle={{ }}
                snapToInterval={ITEM_SIZE}
                snapToAlignment='start'
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrollX } }}],
                    { useNativeDriver: true }
                )}
                onMomentumScrollEnd={onScrollEnd}
                onScrollBeginDrag={onScrollDrag}
                scrollEventThrottle={16}
                CellRendererComponent={({ children, index, style, ...props }) => {
                    const cellStyle = [
                        style,
                        { zIndex: index === currPage ? 1000 : items.length + index },
                    ];
                    return (
                        <View style={cellStyle} index={index} {...props}>
                            {children}
                        </View>
                    );
                }}
                renderItem={({ item, index }) => {
                    if (!item.image) {
                        return <View style={{ width: EMPTY_ITEM_SIZE }} />;
                    }
                    return (<CarouselItem item={item} itemIndex={index} scrollX={scrollX}/>)
                }}
            />
        </View>
    )
}

export default BLCarousel;
