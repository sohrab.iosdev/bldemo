import { StyleSheet } from 'react-native'
import { ITEM_SIZE } from "./index";

export const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    posterImage: {
        width: '100%',
        height: ITEM_SIZE * .5,
        resizeMode: 'cover',
        borderRadius: 24,
        margin: 0,
        marginBottom: 10,
    },
});

export default styles;
