import React from "react";
import { Text, View } from "react-native";
import { ConstantStyles, defaultPadding } from "../../constants";

export const HomeHeader = (props: any) => {
    return (
        <View>
            <View style={[{ height: 130, paddingVertical: defaultPadding, paddingHorizontal: defaultPadding, backgroundColor: "white"}]}>
                <View style={{height: 50}}/>
                <Text style={{fontSize: 30, fontWeight: "600"}}>
                    Tickets
                </Text>
                <Text style={{fontSize: 22, fontWeight: "300", color: "grey"}}>
                    You have 2 active tickets. Good Luck!
                </Text>
            </View>
            <View style={ConstantStyles.shadow }/>
        </View>

    );
}

export default HomeHeader;
