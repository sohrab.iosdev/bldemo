import { StyleSheet } from "react-native";

export const ConstantStyles = StyleSheet.create({
    shadow: {
        borderColor:'#DCDCDC',
        borderWidth: 0.5,
        overflow: 'hidden',
        shadowColor: 'red',
        shadowOffset: { width: 10, height: 10 },
        shadowRadius: 20,
        shadowOpacity: 1,
    }
});

export const defaultPadding = 10;
export const defaultPaddingRegular = 15;
export const defaultPaddingMax = 20;
