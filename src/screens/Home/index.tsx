import React, { Component } from "react";
import { ImageBackground, StyleSheet, View } from "react-native";
import BLCarousel from "../../components/BLCarousel";
import { defaultPaddingMax } from "../../constants";

const data = [
    { key: 'empty-left' },
    {
        id: "1",
        image:
            'https://images.unsplash.com/photo-1567226475328-9d6baaf565cf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
    },
    {
        id: "2",
        image:
            'https://images.unsplash.com/photo-1455620611406-966ca6889d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1130&q=80',
    },
    {
        id: "3",
        image:
            'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    },
    {
        id: "4",
        image:
            'https://images.unsplash.com/photo-1568700942090-19dc36fab0c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    },
    {
        id: "5",
        image:
            'https://images.unsplash.com/photo-1584271854089-9bb3e5168e32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80',
    },
    { key: 'empty-right' }
];

interface Props {
}

interface State {
}

export default class Home extends Component<Props, State> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={ styles.container }>
                <ImageBackground source={require('../../assets/background.png')} style={{flex: 1}}>
                    <View style={{height: defaultPaddingMax}}/>
                    <BLCarousel items={data}/>
                    <View style={{height: defaultPaddingMax}}/>
                    <BLCarousel items={data}/>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});
