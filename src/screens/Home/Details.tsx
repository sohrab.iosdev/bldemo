// @ts-ignore
import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";

interface Props {
}

interface State {
    questions: any[];
    loading: boolean;
}

export default class Details extends Component<Props, State> {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <View style={ styles.container }>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
    },
    text: {
        marginTop: 10,
    }
});
