import React from "react";
import Home from "./src/screens/Home";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Details from "./src/screens/Home/Details";

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
          <Stack.Navigator initialRouteName="Home">
              <Stack.Screen name="Home" component={Home} options={{ title: 'Home' }}/>
              <Stack.Screen name="Details" component={Details} options={{ title: 'Details' }}/>
          </Stack.Navigator>
      </NavigationContainer>
  );
}

